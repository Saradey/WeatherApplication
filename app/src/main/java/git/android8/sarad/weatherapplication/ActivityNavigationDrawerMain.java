package git.android8.sarad.weatherapplication;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import git.android8.sarad.weatherapplication.MyFragment.FragmentFirst;
import git.android8.sarad.weatherapplication.MyFragment.FragmentSettings;
import git.android8.sarad.weatherapplication.MyFragment.FragmentStory;
import git.android8.sarad.weatherapplication.MydbPackage.NoteDataSource;


public class ActivityNavigationDrawerMain extends AppCompatActivity {
    //никто не делает implements NavigationView.OnNavigationItemSelectedListener

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;


    @BindView(R.id.nav_view)
    NavigationView navigationView;

    FragmentFirst fragmentTest;

    @BindString(R.string.position_draw_text)
    String POSITIVE_BUTTON_TEXT;

    private static final String FILENAME = "i2.png";
    //@BindView(R.id.imageView2) ImageView imageView;   почему то выводит ошибку из за butterKnife
    ImageView imageView;
    @BindDrawable(R.drawable.i1)
    Drawable drawable;

    NoteDataSource noteDataSource;


    private EditText nameBox;
    private EditText dateBox;
    private EditText dateBox2;
    private LinearLayout layoutAlterDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        ButterKnife.bind(this);
        imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView_2); //костыльно?

        initUI();
    }


    //иницилизация интерфейса, функция верхнего уровня
    private void initUI() {
        imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView_2); //костыльно?

        initEditBox();
        initLayoutAlterDialog();
        initDataBase();
        initToolbarAndFloatingActionButton();
        initDrawerLayoutAndNavigationLayout();

        //saveDrawableToFile();
        loadDeawableToFile();
    }


    private void initLayoutAlterDialog() {
        layoutAlterDialog = new LinearLayout(ActivityNavigationDrawerMain.this);
        layoutAlterDialog.setOrientation(LinearLayout.VERTICAL);
        layoutAlterDialog.addView(nameBox);
        layoutAlterDialog.addView(dateBox);
        layoutAlterDialog.addView(dateBox2);
    }

    private void initEditBox() {
        nameBox = new EditText(ActivityNavigationDrawerMain.this);
        nameBox.setHint(getString(R.string.header_hint));

        dateBox = new EditText(ActivityNavigationDrawerMain.this);
        dateBox.setHint(getString(R.string.text_mail_send));

        dateBox2 = new EditText(ActivityNavigationDrawerMain.this);
        dateBox2.setHint(getString(R.string.email_send_2));
    }


    private void initDataBase() {
        noteDataSource = new NoteDataSource(getApplicationContext());
        noteDataSource.open();
    }


    //сохраняем изображение на телефон
    private void saveDrawableToFile() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final File file =
                        new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), FILENAME);
                try {
                    Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.i1);
                    FileOutputStream fos = new FileOutputStream(file);
                    bm.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.flush();
                    fos.close();

                    Log.d("MyTest", "Save");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }


    private void loadDeawableToFile() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File file =
                        new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), FILENAME);
                try {
                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    imageView.setImageBitmap(myBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }


    //иницилизиция тулбара
    private void initToolbarAndFloatingActionButton() {
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(getClickListener());
    }

    //селект для тулбара
    @NonNull
    private View.OnClickListener getClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTheMessage();
            }
        };
    }


    private void goToTheMessage() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);
        dialog.setView(layoutAlterDialog);

        dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", dateBox2.getText().toString(), null));

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, nameBox.getText().toString());
                emailIntent.putExtra(Intent.EXTRA_TEXT, dateBox.getText().toString());

                startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email_1)));
            }
        });

        dialog.show();
    }


    private void initDrawerLayoutAndNavigationLayout() {
        // ActionBarDrawerToggle toggle объядиняет в себе дрвайвер лейаут и тулбар помогает определить как навегейт
        //дрвайвер будет наезжать на тулбар и активити
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(getListener());

        /*if (navigationView.getHeaderCount() > 0) {
            //если внутри navigationView есть вью на которые нужно нажимать, то тогда необходимо находить их по
            //getHeaderView
            View header = navigationView.getHeaderView(0);
        }*/
    }

    //нажатие для navigationView
    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home_work:
                        goToTheFragmentThis();
                        break;

                    case R.id.nav_call:
                        goToTheFragmentSettings();

                        break;

                    case R.id.nav_send:
                        sendDevelperThisAplication();
                        break;

                    case R.id.nav_story:
                        goToTheFragmentStory();
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        };
    }

    //кнопка нажатия назад
    @Override
    public void onBackPressed() {
        fragmentTest = null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //надуваем верхние меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    //если нажаоли на верхние меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action) {
            DialogWindow();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //диалоговок окно в котором мы вводим город
    private void DialogWindow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.change_city_dialog));
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(POSITIVE_BUTTON_TEXT, getListenerToShowDialog(input));
        builder.show();
    }

    //OnClickListener для диалогового окна когда мы нажали, передаем фрагменту город
    @NonNull
    private DialogInterface.OnClickListener getListenerToShowDialog(final EditText input) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (fragmentTest != null) {
                    fragmentTest.setCity(input.getText().toString());
                }
            }
        };
    }


    //создаем фрагмент
    private void goToTheFragmentThis() {
        fragmentTest = new FragmentFirst();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_2, fragmentTest);
        transaction.commit();
    }


    //фрагмент истории городов
    private void goToTheFragmentStory() {
        if (fragmentTest != null) fragmentTest = null;
        FragmentStory fragmentStory = new FragmentStory();
        fragmentStory.setNoteDataSource(noteDataSource);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_2, fragmentStory);
        transaction.commit();
    }


    //создаем фрагмент настреок
    private void goToTheFragmentSettings() {
        if (fragmentTest != null) fragmentTest = null;
        FragmentSettings fragmentSettings = new FragmentSettings();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_2, fragmentSettings);
        transaction.commit();
    }


    private void showToast(String show) {
        Toast toast = Toast.makeText(getApplicationContext(),
                show, Toast.LENGTH_SHORT);
        toast.show();
    }


    //сязаться с разработчиком
    private void sendDevelperThisAplication() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String chooserTitle = getString(R.string.chooser_title);
        Intent chosenIntent = Intent.createChooser(intent, chooserTitle);
        startActivity(chosenIntent);
    }


}

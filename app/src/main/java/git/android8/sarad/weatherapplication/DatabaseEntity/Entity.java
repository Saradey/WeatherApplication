package git.android8.sarad.weatherapplication.DatabaseEntity;

public interface Entity {   //на тот случай, если сущностей станет больше
    int getId();

    String getWidgetInfo();
}

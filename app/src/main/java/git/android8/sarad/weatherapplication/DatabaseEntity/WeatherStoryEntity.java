package git.android8.sarad.weatherapplication.DatabaseEntity;


//сущность для базы данных
public class WeatherStoryEntity implements Entity {

    private static final String NAME_CITY = "Name city:";
    private static final String DECRIPTION = " Description:";
    private static final String DATE = " Date:";
    private int id;
    private String nameTown;
    private String description;
    private String myDate;
    private String windy;
    private String teamp;
    private String pressure;
    private String cloudy;
    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getWindy() {
        return windy;
    }

    public void setWindy(String windy) {
        this.windy = windy;
    }

    public String getTeamp() {
        return teamp;
    }

    public void setTeamp(String teamp) {
        this.teamp = teamp;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getCloudy() {
        return cloudy;
    }

    public void setCloudy(String cloudy) {
        this.cloudy = cloudy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameTown() {
        return nameTown;
    }

    public void setNameTown(String nameTown) {
        this.nameTown = nameTown;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMyDate() {
        return myDate;
    }

    public void setMyDate(String myDate) {
        this.myDate = myDate;
    }

    //получаем информацию и вставляем во вью
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME_CITY);
        stringBuilder.append(nameTown);
        stringBuilder.append("\n");
        stringBuilder.append(DECRIPTION);
        stringBuilder.append(description);
        stringBuilder.append("\n");
        stringBuilder.append(DATE);
        stringBuilder.append(myDate);
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }


    @Override
    public String getWidgetInfo() {
        String teamp = nameTown + "\n" + description + "\n" + this.teamp;
        return teamp;
    }
}

package git.android8.sarad.weatherapplication.DatabaseEntity;


//сущность для создания и работы с таболицей WeatherTown
public class WeatherTown {

    private static final String COLUMN_ID = "id_weather";
    private static final String COLUMN_TOWN = "town";
    private static final String COLUMN_DESCRIPTION = "descript";
    private static final String COLUMN_WIND = "wind";
    private static final String COLUMN_TEAMP = "temp";
    private static final String COLUM_PRESSURE = "pressure";
    private static final String COLUMN_CLOUDS = "clouds";
    private static final String TABLE_NAME = "WeatherTown";
    private static final String DATE_NOW = "DATE_NOW";
    private static final String ID_ICON = "id_icon";



    public String getColumnId() {
        return COLUMN_ID;
    }

    public String getColumnTown() {
        return COLUMN_TOWN;
    }

    public String getColumnDescription() {
        return COLUMN_DESCRIPTION;
    }

    public String getColumnWind() {
        return COLUMN_WIND;
    }

    public String getColumnTeamp() {
        return COLUMN_TEAMP;
    }

    public String getColumPressure() {
        return COLUM_PRESSURE;
    }

    public String getColumnClouds() {
        return COLUMN_CLOUDS;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public String getDateNow() {
        return DATE_NOW;
    }

    public String getIdIcon() {
        return ID_ICON;
    }
}

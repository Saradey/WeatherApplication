package git.android8.sarad.weatherapplication.Functional;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;

import org.json.JSONObject;

import git.android8.sarad.weatherapplication.InternetDonlowd.Loader;
import git.android8.sarad.weatherapplication.InternetDonlowd.WeatherInfo;
import git.android8.sarad.weatherapplication.MyFragment.FragmentFirst;
import git.android8.sarad.weatherapplication.MydbPackage.NoteDataSource;


public class TimerService extends Service {

    private static final String TAG = "MyTimerService";

    private static final String CITY_INDEX_SAVE = "city";
    public ExampleBinder binder = new ExampleBinder();
    FragmentFirst fragmentFirst;
    private String citi_name;
    private NoteDataSource noteDataSource;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");

        initDataBase();
    }


    void schedule() {
        Log.d(TAG, "schedule: ");

        new Thread() {
            public void run() {
                if (citi_name != null) {
                    Log.d(TAG, "add entity");
                    JSONObject json = Loader.getJSONData(citi_name);

                    if (json != null) {
                        WeatherInfo weather = new Gson().fromJson(json.toString(), WeatherInfo.class);
                        noteDataSource.addWeather(weather);
                    }
                }
            }
        }.start();
    }


    public void initDataBase() {
        noteDataSource = new NoteDataSource(getApplicationContext());
        noteDataSource.open();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            citi_name = intent.getStringExtra(CITY_INDEX_SAVE);
        }

        schedule();
        return super.onStartCommand(intent, flags, startId);
    }


    public void setFragmentParent(FragmentFirst fragmentFirst) {
        this.fragmentFirst = fragmentFirst;
    }


    public void onBindService(View view) {
        Log.d(TAG, "onBindService: ");
    }


    public void onUnbindService(View view) {
        Log.d(TAG, "onUnbindService: ");
    }


    @Override
    public void onDestroy() {
        Log.w(TAG, "onDestroy: ");
    }


    public class ExampleBinder {
        public TimerService getService() {
            return TimerService.this;
        }
    }


}

package git.android8.sarad.weatherapplication.InternetDonlowd;


import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


//здесь мы по api загружаем с бекэнда данные и серелизуем их
public class Loader {

    private static final String appId = "ed0b85993b3d08bde3261fffb5066b1e";
    private static final String OPEN_WEATHER_MAP_API = "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s";
    private static final String RESPONSE = "cod";
    private static final String NEW_LINE = "\n";
    private static final int ALL_GOOD = 200;

    //обращаемся на сервер
    public static JSONObject getJSONData(String city) {

        try {
            URL url = new URL(String.format(OPEN_WEATHER_MAP_API, city, appId));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder rawData = new StringBuilder(1024);
            String tempVariable;
            while ((tempVariable = reader.readLine()) != null) {
                rawData.append(tempVariable).append(NEW_LINE);
            }
            reader.close();


            JSONObject jsonObject = new JSONObject(rawData.toString());
            // API openweathermap
            if (jsonObject.getInt(RESPONSE) != ALL_GOOD) {
                return null;
            }
            return jsonObject;
        } catch (Exception e) {
            Log.d("MyExeption", e.getMessage());
            return null;
        }
    }

}

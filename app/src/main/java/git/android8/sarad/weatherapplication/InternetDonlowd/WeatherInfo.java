package git.android8.sarad.weatherapplication.InternetDonlowd;


import com.google.gson.annotations.SerializedName;

import java.util.List;


//для загрузки и серилизации данных
public class WeatherInfo {


    @SerializedName("name")
    private String name;

    @SerializedName("clouds")
    private Clouds clouds;

    @SerializedName("wind")
    private Wind wind;

    @SerializedName("weather")
    private List<WeatherItem> weather;

    @SerializedName("main")
    private Main main;


    public String getName() {
        return name;
    }


    class Clouds {
        @SerializedName("all")
        Integer all;
    }

    public String getClouds() {
        return String.valueOf(clouds.all);
    }


    private class Wind {

        @SerializedName("speed")
        Double speed;
        @SerializedName("deg")
        Double deg;

        @Override
        public String toString() {
            return "Wind{" +
                    "speed=" + speed +
                    ", deg=" + deg +
                    '}';
        }
    }

    public String getWind() {
        return wind.toString();
    }

    public String getTemp() {
        return String.valueOf(main.temp);
    }

    public String getPressure() {
        return String.valueOf(main.pressure);
    }

    public String getMain() {
        if (weather.size() > 0) {
            return weather.get(0).main;
        } else {
            return "";
        }
    }


    private class WeatherItem {
        @SerializedName("main")
        String main;
        @SerializedName("description")
        String description;
        @SerializedName("icon")
        String icon;
    }

    public String getDesc() {
        if (weather.size() > 0) {
            return weather.get(0).description;
        } else {
            return "";
        }
    }

    public String getIcon() {
        if (weather.size() > 0) {
            return weather.get(0).icon;
        } else {
            return "";
        }
    }

    private class Main {
        @SerializedName("temp")
        Double temp;
        @SerializedName("pressure")
        Double pressure;
    }


}

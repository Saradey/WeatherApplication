package git.android8.sarad.weatherapplication.MyFragment;


import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.content.ServiceConnection;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import git.android8.sarad.weatherapplication.DatabaseEntity.WeatherStoryEntity;
import git.android8.sarad.weatherapplication.Functional.TimerService;
import git.android8.sarad.weatherapplication.InternetDonlowd.Loader;
import git.android8.sarad.weatherapplication.InternetDonlowd.WeatherInfo;
import git.android8.sarad.weatherapplication.MydbPackage.NoteDataSource;
import git.android8.sarad.weatherapplication.R;


//первый фрагмент, здесь выводим информации только что полученную с сервера
public class FragmentFirst extends Fragment {

    private static final String TAG = "MyTimerService";

    @BindView(R.id.city_field2)
    TextView city_field2;

    @BindView(R.id.weather2)
    TextView weather2;

    @BindView(R.id.windy2)
    TextView windy2;

    @BindView(R.id.temp2)
    TextView temp2;

    @BindView(R.id.press2)
    TextView press2;

    private final Handler handler = new Handler();

    @BindView(R.id.cloud2)
    TextView cloud2;

    @BindView(R.id.weather_icon)
    ImageView weatherIcon;

    private static final String CITY_INDEX_SAVE = "city";
    private String citi_name;

    private NoteDataSource noteDataSource;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragmant_first, container, false);

        initDataBase();

        ButterKnife.bind(this, view);

        loadCity();

        return view;
    }


    public void initDataBase() {
        noteDataSource = new NoteDataSource(getActivity().getApplicationContext());
        noteDataSource.open();
    }


    public void onStartService() {
        getActivity().startService(new Intent(getActivity(), TimerService.class).putExtra(CITY_INDEX_SAVE, citi_name));
    }


    private void loadCity() {
        SharedPreferences pref = getActivity().getPreferences(Context.MODE_PRIVATE);
        if (pref.contains(CITY_INDEX_SAVE)) {
            citi_name = pref.getString(CITY_INDEX_SAVE, "");
            //loadWeatherApiAndSetView(loadedPrefs);
            getWeatherForDBAndReander();
        }
    }

    public NoteDataSource getNoteDataSource() {
        return noteDataSource;
    }

    //обращаемся на сервер за данными, затем эти данные вставляем во вьюшки
    public void setCity(final String nameCity) {
        saveCity(nameCity);
        citi_name = nameCity;
        //loadWeatherApiAndSetView(nameCity);
        getWeatherForDBAndReander();
        loadWeatherApiAndSetView(nameCity);
    }



    public void setNoteDataSource(NoteDataSource noteDataSource) {
        this.noteDataSource = noteDataSource;
    }


    private void loadWeatherApiAndSetView(final String nameCity) {
        new Thread() {
            public void run() {
                final JSONObject json = Loader.getJSONData(nameCity);

                if (json == null) {
                    handler.post(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.place_not_found),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    try {
                        final WeatherInfo weather = new Gson().fromJson(json.toString(), WeatherInfo.class);
                        noteDataSource.addWeather(weather); //добавление в базу данных
                        handler.post(new Runnable() {
                            public void run() {
                                renderWeather(weather);
                            }
                        });
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }


    private void saveCity(String nameCity) {
        SharedPreferences pref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(CITY_INDEX_SAVE, nameCity);
        editor.apply();
    }


    public void renderWeather(WeatherInfo weather) {
            city_field2.setText(weather.getName());
            String teamp = weather.getMain() + " " + weather.getDesc();
            weather2.setText(teamp);
            windy2.setText(weather.getWind());
            temp2.setText(weather.getTemp());
            cloud2.setText(weather.getClouds());
            press2.setText(weather.getPressure());
            setWeatherIcon(weather.getIcon());
    }


    public void getWeatherForDBAndReander() {
        onStartService();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                WeatherStoryEntity weatherStoryEntity = noteDataSource.getWeatherEntity();
                city_field2.setText(weatherStoryEntity.getNameTown());
                weather2.setText(weatherStoryEntity.getDescription());
                windy2.setText(weatherStoryEntity.getWindy());
                temp2.setText(weatherStoryEntity.getTeamp());
                cloud2.setText(weatherStoryEntity.getCloudy());
                press2.setText(weatherStoryEntity.getPressure());
            }
        }, 1000);
    }


    private void setWeatherIcon(String iconCode) {
        Drawable drawable;
        switch (iconCode) {
            case "01d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_01d, null);
                break;
            case "01n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_01n, null);
                break;
            case "02d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_02d, null);
                break;
            case "02n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_02n, null);
                break;
            case "03d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_03d, null);
                break;
            case "03n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_03n, null);
                break;
            case "04d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_04d, null);
                break;
            case "04n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_04n, null);
                break;
            case "09d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_09d, null);
                break;
            case "09n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_09n, null);
                break;
            case "10d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_10d, null);
                break;
            case "10n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_10n, null);
                break;
            case "11d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_11d, null);
                break;
            case "11n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_11n, null);
                break;
            case "13d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_13d, null);
                break;
            case "13n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_13n, null);
                break;
            case "50d":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_50d, null);
                break;
            case "50n":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_50n, null);
                break;
            default:
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icon_01d, null);
                break;
        }
        weatherIcon.setImageDrawable(drawable);
    }
}

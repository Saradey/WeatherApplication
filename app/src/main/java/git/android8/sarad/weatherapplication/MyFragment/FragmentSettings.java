package git.android8.sarad.weatherapplication.MyFragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import git.android8.sarad.weatherapplication.R;


//TODO сделать смену темы
public class FragmentSettings extends Fragment {


    @BindView(R.id.radioGroup_1)
    RadioGroup radioGroup;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_settings, container, false);

        ButterKnife.bind(this, view);
        initUI();


        return view;
    }


    private void initUI() {
        radioGroup.setOnCheckedChangeListener(getListenerRadioGroup());
    }


    @NonNull
    private RadioGroup.OnCheckedChangeListener getListenerRadioGroup() {
        return new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioButton1:

                        break;

                    case R.id.radioButton2:

                        break;

                    case R.id.radioButton3:

                        break;

                    default:
                        break;
                }
            }
        };
    }


}

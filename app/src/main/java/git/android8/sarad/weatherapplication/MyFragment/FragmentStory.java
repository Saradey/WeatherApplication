package git.android8.sarad.weatherapplication.MyFragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import git.android8.sarad.weatherapplication.DatabaseEntity.Entity;
import git.android8.sarad.weatherapplication.MydbPackage.NoteDataSource;
import git.android8.sarad.weatherapplication.R;


//здесь наш список базы данных
public class FragmentStory extends Fragment {

    @BindView(R.id.list)
    ListView listView;
    ArrayAdapter<Entity> arrayAdapter;
    List<Entity> myStoryList;
    private NoteDataSource noteDataSource;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_story, container, false);
        ButterKnife.bind(this, view);

        initUI();
        return view;
    }


    private void initUI() {
        initListView();
    }


    private void initListView() {
        myStoryList = noteDataSource.getAllStory();
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
        listView.setAdapter(arrayAdapter);
        registerForContextMenu(listView);
    }


    public void setNoteDataSource(NoteDataSource noteDataSource) {
        this.noteDataSource = noteDataSource;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_edit:
                editElement(info.position);
                return true;
            case R.id.menu_delete:
                deleteElement(info.position);
                return true;
        }
        return super.onContextItemSelected(item);
    }


    private void editElement(int id) {
        noteDataSource.editEntity(myStoryList.get(id).getId());
        dataUpdated();
    }


    private void deleteElement(int id) {
        noteDataSource.deleteEntity(myStoryList.get(id));
        dataUpdated();
    }


    private void dataUpdated() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllStory());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }


}

package git.android8.sarad.weatherapplication.MydbPackage;


import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import git.android8.sarad.weatherapplication.DatabaseEntity.WeatherTown;


//здесь мы создаем базу данных и обновляем ее
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "information.db";
    private WeatherTown weatherTown;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        weatherTown = new WeatherTown();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE "
                    + weatherTown.getTableName()
                    + " ("
                    + weatherTown.getColumnId()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + weatherTown.getColumnTown()
                    + " TEXT, "
                    + weatherTown.getColumnDescription()
                    + " TEXT,"
                    + weatherTown.getColumnWind()
                    + " TEXT,"
                    + weatherTown.getColumnTeamp()
                    + " REAL,"
                    + weatherTown.getColumPressure()
                    + " REAL,"
                    + weatherTown.getColumnClouds()
                    + " INTEGER,"
                    + weatherTown.getDateNow()
                    + " INTEGER,"
                    + weatherTown.getIdIcon()
                    + " TEXT"
                    + ");");
        } catch (SQLException e) {
            Log.d("MyTest", "Eror create db");
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //здесь изменения версии
    }

    public WeatherTown getWeatherTown() {
        return weatherTown;
    }

}

package git.android8.sarad.weatherapplication.MydbPackage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import git.android8.sarad.weatherapplication.DatabaseEntity.Entity;
import git.android8.sarad.weatherapplication.DatabaseEntity.WeatherStoryEntity;
import git.android8.sarad.weatherapplication.DatabaseEntity.WeatherTown;
import git.android8.sarad.weatherapplication.InternetDonlowd.WeatherInfo;


//здесь мы добавляем, удаляем, обновляем
public class NoteDataSource {

    private DatabaseHelper dbHelper;
    private SQLiteDatabase database;

    private WeatherTown weatherTown;

    private static final String TAG = "DateBase";


    public NoteDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
        weatherTown = dbHelper.getWeatherTown();
    }


    public void addWeather(WeatherInfo weatherInfo) {

        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(weatherTown.getColumnTown(), weatherInfo.getName());
        values.put(weatherTown.getColumnDescription(), weatherInfo.getDesc());
        values.put(weatherTown.getColumnWind(), weatherInfo.getWind());
        values.put(weatherTown.getColumnTeamp(), weatherInfo.getTemp());
        values.put(weatherTown.getColumPressure(), weatherInfo.getPressure());
        values.put(weatherTown.getColumnClouds(), weatherInfo.getClouds());
        values.put(weatherTown.getDateNow(), System.currentTimeMillis());
        values.put(weatherTown.getIdIcon(), weatherInfo.getIcon());


        database.insert(weatherTown.getTableName(), null, values);  //DatabaseHelper.TABLE_NOTES название таблицы
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public List<Entity> getAllStory() {
        List<Entity> story = new ArrayList<>();
        String[] allColumn = {
                weatherTown.getColumnId(),
                weatherTown.getColumnTown(),
                weatherTown.getColumnDescription(),
                weatherTown.getDateNow(),
        };

        Cursor cursor = database.query(weatherTown.getTableName(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Entity note = cursorToWeatherStory(cursor);
            story.add(note);
            cursor.moveToNext();
        }

        cursor.close();
        return story;
    }


    public WeatherStoryEntity getWeatherEntity() {

        String[] allColumn = {
                weatherTown.getColumnId(),
                weatherTown.getColumnTown(),
                weatherTown.getColumnDescription(),
                "MAX(" + weatherTown.getDateNow() + ")",
                weatherTown.getColumnWind(),
                weatherTown.getColumnTeamp(),
                weatherTown.getColumPressure(),
                weatherTown.getColumnClouds(),
                weatherTown.getIdIcon()
        };

        Cursor cursor = database.query(weatherTown.getTableName(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        WeatherStoryEntity note = (WeatherStoryEntity) addWetherEntityToReander(cursor);

        Log.d(TAG, "get reander weather");
        cursor.close();
        return note;
    }


    private Entity addWetherEntityToReander(Cursor cursor) {
        WeatherStoryEntity entity = new WeatherStoryEntity();
        entity.setId(cursor.getInt(0));
        entity.setNameTown(cursor.getString(1));
        entity.setDescription(cursor.getString(2));
        entity.setMyDate(cursor.getString(3));
        entity.setWindy(cursor.getString(4));
        entity.setTeamp(cursor.getString(5));
        entity.setPressure(cursor.getString(6));
        entity.setCloudy(cursor.getString(7));
        entity.setIcon(cursor.getString(8));
        return entity;
    }


    //создаем наш класс которые будет отображаться в вью
    private Entity cursorToWeatherStory(Cursor cursor) {
        WeatherStoryEntity entity = new WeatherStoryEntity();
        entity.setId(cursor.getInt(0));
        entity.setNameTown(cursor.getString(1));
        entity.setDescription(cursor.getString(2));
        entity.setMyDate(cursor.getString(3));
        return entity;
    }


    public void editEntity(int id) {
        ContentValues editedNote = new ContentValues();
        editedNote.put(weatherTown.getColumnTown(), "Edit");
        editedNote.put(weatherTown.getColumnDescription(), "Edit");
        editedNote.put(weatherTown.getDateNow(), "Edit");

        database.update(weatherTown.getTableName(),
                editedNote,
                weatherTown.getColumnId() + "=" + id,
                null);
    }


    public void deleteEntity(Entity entity) {
        long id = entity.getId();
        database.delete(weatherTown.getTableName(), weatherTown.getColumnId()
                + " = " + id, null);
    }


}

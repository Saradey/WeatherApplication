package git.android8.sarad.weatherapplication.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class NotesWidgetService extends RemoteViewsService {


    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new WidgetNotesFactory(this.getApplicationContext(), intent);
    }


}

package git.android8.sarad.weatherapplication.widget;


import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;
import java.util.List;

import git.android8.sarad.weatherapplication.DatabaseEntity.Entity;
import git.android8.sarad.weatherapplication.DatabaseEntity.WeatherStoryEntity;
import git.android8.sarad.weatherapplication.MydbPackage.NoteDataSource;
import git.android8.sarad.weatherapplication.R;

//фабрика объектов
public class WidgetNotesFactory implements RemoteViewsService.RemoteViewsFactory {

    Context mContext;
    private List<Entity> records;
    private int icon_id;

    public WidgetNotesFactory(Context context, Intent intent) {
        mContext = context;
    }

    //вызывается когда адаптор создается
    public void onCreate() {
        records = new ArrayList<>();
    }


    @Override
    public int getCount() {
        return records.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    //это значит что мы будем использовать самую стандартную вьюшки при загрузки
    @Override
    public RemoteViews getLoadingView() {
        return null;
    }


    //создает какую то вьюшку
    @Override
    public RemoteViews getViewAt(int position) {

        RemoteViews rView = new RemoteViews(mContext.getPackageName(),
                R.layout.item_widget);

        rView.setTextViewText(R.id.text1000, records.get(position).getWidgetInfo());
        rView.setImageViewResource(R.id.my_image, icon_id);

        Intent click = new Intent();
        click.putExtra(WidgetNotes.NOTE_TEXT, records.get(position).toString());
        rView.setOnClickFillInIntent(R.id.text1000, click);

        return rView;
    }


    //какое количество элементов разных типов может быть
    @Override
    public int getViewTypeCount() {
        return 1;
    }

    //
    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public void onDataSetChanged() {
        records.clear();
        NoteDataSource noteDataSource = new NoteDataSource(mContext);
        noteDataSource.open();

        WeatherStoryEntity weatherStoryEntity = noteDataSource.getWeatherEntity();
        icon_id = getAtherIcon(weatherStoryEntity.getIcon());
        records.add(weatherStoryEntity);

        noteDataSource.close();
    }


    @Override
    public void onDestroy() {
    }


    private int getAtherIcon(String iconCode) {

        switch (iconCode) {
            case "01d":
                return R.drawable.icon_01d;
            case "01n":
                return R.drawable.icon_01n;
            case "02d":
                return R.drawable.icon_02d;
            case "02n":
                return R.drawable.icon_02n;
            case "03d":
                return R.drawable.icon_03d;
            case "03n":
                return R.drawable.icon_03n;
            case "04d":
                return R.drawable.icon_04d;
            case "04n":
                return R.drawable.icon_04n;
            case "09d":
                return R.drawable.icon_09d;
            case "09n":
                return R.drawable.icon_09n;
            case "10d":
                return R.drawable.icon_10d;
            case "10n":
                return R.drawable.icon_10n;
            case "11d":
                return R.drawable.icon_11d;
            case "11n":
                return R.drawable.icon_11n;
            case "13d":
                return R.drawable.icon_13d;
            case "13n":
                return R.drawable.icon_13n;
            case "50d":
                return R.drawable.icon_50d;
            case "50n":
                return R.drawable.icon_50n;
            default:
                return R.drawable.icon_01d;
        }

    }

}